<?php

/**
 * @file
 * Views hook to add this field unger Custom global category.
 */

/**
 * Implements hook_views_data().
 */
function multivalue_click_sort_views_data() {

  $data['views']['table']['group'] = t('Global');
  $data['views']['table']['join'] = [
    '#global' => [],
  ];

  $data['views']['multivalue_click_sort'] = [
    'title' => t('Multivalue click sort field'),
    'field' => [
      'id' => 'multivalueclicksort',
    ],
  ];

  return $data;
}
