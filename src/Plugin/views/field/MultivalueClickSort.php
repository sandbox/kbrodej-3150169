<?php

namespace Drupal\multivalue_click_sort\Plugin\views\field;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\ViewsHandlerManager;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides MultivalueClickSort field handler.
 *
 * @ViewsField("multivalueclicksort")
 */
class MultivalueClickSort extends FieldPluginBase {

  // TODO: handle using this functionality only on table view style.
  // TODO: implement render as link for each concatenated entity value.

  /**
   * Bundle of the field.
   *
   * @var string
   */
  private $bundle;

  /**
   * Entity type id of which field belongs to.
   *
   * @var string
   */
  private $entityTypeId;

  /**
   * Machine id of the selected field.
   *
   * @var string
   */
  private $fieldName;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Views handler manager.
   *
   * @var \Drupal\views\Plugin\ViewsHandlerManager
   */
  protected $viewsHandlerManager;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.views.join')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityFieldManagerInterface $entity_field_manager, ViewsHandlerManager $views_handler_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->bundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
    $this->viewsHandlerManager = $views_handler_manager;
  }

  /**
   * {@inheritDoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['field_overlay'] = ['default' => []];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    $form['field_overlay'] = [
      '#type' => 'select',
      '#title' => $this->t('Test'),
      '#options' => $this->getFormOptions(),
      '#default_value' => $this->options['field_overlay'],
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    if ($selected_value = explode(':', $form_state->getValues()['options']['field_overlay'])) {
      $this->entityTypeId = $selected_value[0];
      $this->bundle = $selected_value[1];
      $this->fieldName = $selected_value[2];
      $this->options['field_overlay'] = $this->entityTypeId . ':' . $this->bundle . ':' . $this->fieldName;
    }
    parent::submitOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    // TODO: handle special cases:
    // - taxonomy.
    // - user.
    // - relation to custom entity.
    $target_field = $this->explodeFieldInfo();
    $_entity = $values->_entity;
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $this->entityTypeManager->getStorage($_entity->getEntityTypeId())->load($_entity->id());
    if ($entity->hasField($target_field['field_id']) && $entity->getFieldDefinition($target_field['field_id'])->getType() === 'entity_reference') {
      $value = [];
      $referenced_entities = $entity->get($target_field['field_id'])->referencedEntities();
      foreach ($referenced_entities as $referenced_entity) {
        $value[] = $referenced_entity->getTitle();
        sort($value);
      }
      return implode(', ', $value);
    }
    else {
      $this->messenger()->addError($this->t("Field @target_field does not exist on entity type @entity_type", [
        '@target_field' => mb_strtoupper($target_field['field_id']),
        '@entity_type' => mb_strtoupper($target_field['entity_type_id']),
      ]));
      return parent::render($values);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function query() {
    $configuration = $this->getQueryDetails();
    $new_field = 'title';
    $join = $this->viewsHandlerManager->createInstance('standard', $configuration);
    $this->query->addTable('node_field_data', NULL, $join, 'mvs_' . $this->field);
    $formula = "GROUP_CONCAT(DISTINCT(mvs_multivalue_click_sort.$new_field) SEPARATOR ', ')";
    $this->query->addField(NULL, $formula, $this->field);
    $this->query->addTag('multivalue_click_sort_dynamic_query');

  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function clickSort($order) {
    $this->query->addOrderBy(NULL, NULL, $order, 'multivalue_click_sort');
    $this->query->addTag('multivalue_click_sort_dynamic_query');
  }

  /**
   * Provides the handler some groupby.
   */
  public function usesGroupBy() {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  private function getFormOptions() {
    $definitions = \Drupal::entityTypeManager()->getDefinitions();
    $options = [];
    foreach ($definitions as $entity_type_id => $entity_type) {
      if (is_subclass_of($entity_type->getClass(), FieldableEntityInterface::class)
      && is_subclass_of($entity_type->getClass(), ContentEntityInterface::class)) {
        $bundles = $this->bundleInfo->getBundleInfo($entity_type_id);
        foreach ($bundles as $bundle_id => $label) {
          $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle_id);
          foreach (array_keys($field_definitions) as $array_key) {
            if ($field_definitions[$array_key]->getType() === 'entity_reference') {
              $field_machine_name = $field_definitions[$array_key]->getName();
              $field_label = $field_definitions[$array_key]->getLabel();
              if (!array_key_exists($field_machine_name, $options[$entity_type_id])) {
                $options[$entity_type_id][$entity_type_id . ':' . $bundle_id . ':' . $field_machine_name] = $bundle_id . ':' . $field_label;
              }
            }
          }
        }
      }
    }
    return $options;
  }

  /**
   * Explodes submitted field into associative array.
   *
   * @return array
   *   Associative array of settings.
   */
  private function explodeFieldInfo() {
    $indexed = explode(':', $this->options['field_overlay']);
    return [
      'entity_type_id' => $indexed[0],
      'bundle_id' => $indexed[1],
      'field_id' => $indexed[2],
    ];
  }

  /**
   * Gets the base tables and field tables for join.
   *
   * @return array
   *   Array of configurations for creating new table join.
   */
  private function getQueryDetails() {

    // TODO: handle special cases:
    // - taxonomy.
    // - user.
    // - relation to custom entity.
    $field_info = $this->explodeFieldInfo();

    $view_base_table = $this->view->getBaseTables();
    $base_table = '';
    foreach ($view_base_table as $base_table_id => $bool) {
      if ($bool && $base_table_id !== '#global') {
        $base_table = $base_table_id;
      }
    }

    /** @var \Drupal\field\Entity\FieldStorageConfig $field_table */
    $field_table = FieldStorageConfig::loadByName($field_info['entity_type_id'], $field_info['field_id']);
    $configuration['table'] = $base_table;
    $configuration['left_table'] = str_replace('.', '__', $field_table->id());
    // Assuming entity_reference fields have _target_id at the end.
    $configuration['left_field'] = $field_table->getName() . '_target_id';
    $configuration['field'] = 'nid';

    return $configuration;
  }

}
